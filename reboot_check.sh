#!/bin/bash
###############################################################
# _______Author : ASSOULINE JORDAN________________            #
#  Version : 1.1 - 22/07/2016                                 #
#  reboot_check.sh                                            #
###########################################################################
# Simple bash scripts wich do tasks :                                     #
# - Check if the user uses the script correctly with the right parameters #
# - First loop which check when the host becomes not reachable anymore    #
# - Second loop which check when the host comes back up                   #
###################################################################################
# Input :                                                                         #
# - host (string) : The host IP or hostname                                       #
# - count (int) : Number of packet used for each reachability test (5 by default) #
###################################################################################

host=$1
count=$2

################
# Number regex #
################
re='^[0-9]+$'

#########################################################################
# Check if the user uses the script correctly with the right parameters #
#########################################################################
if [ -z $1 ]; 
then
        echo 'Warning, you have not specified any host'
        echo 'Correct use : "reboot_check.sh host count"'
        exit 8
fi

if [ -z $2 ]; then
        echo 'Warning, you have not specified any count'
        echo 'Correct use : "reboot_check.sh host count"'
        echo 'The value 5 will be used by default'
        count=5
fi

######################################################################
# First loop which check when the host becomes not reachable anymore #
######################################################################
i=0
while [ $i -eq 0 ]
do
        packet_number=$(ping -c $count $host 2> /etc/reboot.log | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
        if ! [[ $packet_number =~ $re ]]; then
                        echo "Host : $host is down (ping failed) at $(date)"
                        i=1

        elif [ $packet_number -eq 0 ]; then
                        echo "Host : $host is down (ping failed) at $(date)"
                        i=1

        elif [ $packet_number -eq $count ]; then
                        echo "The Host : $host is up and all packets was received. Go loop until he goes down"
                        sleep 5
                        i=0

        else
                        echo "The Host seems to be reachable, but the connectivity is bad. Go loop until he goes down"
                        sleep 5
                        i=0
        fi
done

#######################################################
# Second loop which check when the host comes back up #
#######################################################
i=0
while [ $i -eq 0 ]
do
        packet_number=$(ping -c $count $host 2> /etc/reboot.log | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
        if ! [[ $packet_number =~ $re ]]; then
                        echo "Host : $host is down (ping failed) at $(date). Go loop until he comes back up"
                        sleep 5
                        i=0

        elif [ $packet_number -eq 0 ]; then
                        echo "Host : $host is down (ping failed) at $(date).Go loop until he comes back up"
                        sleep 5
                        i=0

        elif [ $packet_number -eq $count ]; then
                        echo "The Host : $host is up and all packets was received."
                        i=1

        else
                        echo "The Host seems to be reachable, but the connectivity is bad"
                        i=1
        fi
done

echo "The host : $host is correctly rebooted"